# Auraliving Search Proxy

This app is intended to be used only temporarily and as a last result.
The `vtex.search-graphql` should be the default and this app should only be used to replace the `productSearch` query to solve the reported issues.
