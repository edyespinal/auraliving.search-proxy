import type { RecorderState, ServiceContext } from '@vtex/api'

import type { Clients } from '../clients'
import type { IOServiceLogger } from '../utils/errorHandling/ioServiceLogger'

declare global {
  type Context = ServiceContext<Clients, State>

  interface State extends RecorderState {
    serviceLogger: IOServiceLogger
  }
}
