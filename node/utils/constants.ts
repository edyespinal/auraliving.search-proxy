export const ORDER_BY = {
  PRICE_DESC: 'OrderByPriceDESC',
  PRICE_ASC: 'OrderByPriceASC',
  TOP_SALE_DESC: 'OrderByTopSaleDESC',
  REVIEW_RATE_DESC: 'OrderByReviewRateDESC',
  NAME_ASC: 'OrderByNameASC',
  NAME_DESC: 'OrderByNameDESC',
  RELEASE_DATE_DESC: 'OrderByReleaseDateDESC',
  BEST_DISCOUNT_DESC: 'OrderByBestDiscountDESC',
  SCORE_DESC: 'OrderByScoreDESC',
} as const
