export function removeDuplicateProducts(products: any[]) {
  const seen = new Set()

  const filteredProducts = products.filter((product) => {
    const id = product.productId

    return seen.has(id) ? false : seen.add(id)
  })

  return filteredProducts
}

export function filterProductsOutOfPriceRange(products: any[], args: any) {
  let { priceRange } = args

  if (!priceRange) {
    for (const facet of args.selectedFacets) {
      if (facet.key === 'priceRange') {
        priceRange = facet.value

        break
      }
    }
  }

  if (!priceRange) {
    return products
  }

  const [from, to] = priceRange.split(' TO ')

  return products.filter((product) => {
    const price = product.items[0].sellers[0].commertialOffer.Price

    return price >= Number(from) && price <= Number(to)
  })
}
