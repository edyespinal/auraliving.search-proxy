import { ORDER_BY } from './constants'

export function sortProducts(products: any, orderBy: string) {
  if (orderBy === ORDER_BY.PRICE_DESC) {
    products.sort((a: any, b: any) => {
      return (
        b.items[0].sellers[0].commertialOffer.Price -
        a.items[0].sellers[0].commertialOffer.Price
      )
    })
  }

  if (orderBy === ORDER_BY.PRICE_ASC) {
    products.sort((a: any, b: any) => {
      return (
        a.items[0].sellers[0].commertialOffer.Price -
        b.items[0].sellers[0].commertialOffer.Price
      )
    })
  }

  return products
}
