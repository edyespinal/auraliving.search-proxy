function getQueryArguments(query: string) {
  const args = query
    .substring(query.indexOf('(') + 1, query.indexOf(')'))
    .replace(/(\w+):/g, '"$1":')

  const parsedArgs = JSON.parse(`{${args}}`)

  return parsedArgs
}

const queryTypes = {
  productSearch: 'productSearch',
  facets: 'facets',
} as const

type QueryTypes =
  | (typeof queryTypes)[keyof typeof queryTypes]
  | 'queryNotSupported'

function getQueryName(query: string): QueryTypes {
  let queryName: QueryTypes | null = null

  for (const [key, value] of Object.entries(queryTypes)) {
    if (query.includes(key)) {
      queryName = value

      break
    }
  }

  if (!queryName) {
    return 'queryNotSupported'
  }

  return queryName
}

export function getQueryInfo(query: string) {
  const args = getQueryArguments(query)
  const queryName = getQueryName(query)

  return { args, queryName }
}
