import { LINKED } from '@vtex/api'

import type { IOServiceError } from './ioServiceError'
import { LOGS_MAX_SIZE, SERVICE_LOGS_BUCKET, SERVICE_LOGS } from './constants'
import type {
  IOServiceLogs,
  InfoLog,
} from '../../typings/errorHandling/ioServiceLogger'

export class IOServiceLogger {
  constructor(context: Context) {
    this.context = context
  }

  private context: Context

  /**
   * @param ioServiceError
   * @description This method logs the error to OpenSearch and to the console if the app is linked
   */
  public async exception(ioServiceError: IOServiceError) {
    if (LINKED) {
      console.error(ioServiceError)
    }

    this.context.vtex.logger.error(ioServiceError)
  }

  /**
   * @param ioServiceError
   * @description This method adds the error to the application logs
   */
  public async error(ioServiceError: IOServiceError) {
    try {
      if (LINKED) {
        console.error(ioServiceError)
      }

      let appLogs = await this.getAppLogs()

      if (!appLogs) {
        appLogs = []
      }

      const updatedAppLogs = [ioServiceError, ...appLogs].slice(
        0,
        LOGS_MAX_SIZE
      )

      await this.updateAppLogs(updatedAppLogs)
    } catch (error) {
      console.error('Error updating app logs', error)
    }
  }

  /**
   * @param data
   * @description This method adds the data to the application logs
   */
  public async info(data: InfoLog) {
    try {
      if (LINKED) {
        console.info(data)
      }

      let appLogs = await this.getAppLogs()

      if (!appLogs) {
        appLogs = []
      }

      const updatedAppLogs = [data, ...appLogs].slice(0, LOGS_MAX_SIZE)

      await this.updateAppLogs(updatedAppLogs)
    } catch (error) {
      console.error('Error updating app logs', error)
    }
  }

  private getAppLogs() {
    return this.context.clients.vbase.getJSON<IOServiceLogs>(
      SERVICE_LOGS_BUCKET,
      SERVICE_LOGS,
      true
    )
  }

  private updateAppLogs(appLogs: IOServiceLogs) {
    return this.context.clients.vbase.saveJSON(
      SERVICE_LOGS_BUCKET,
      SERVICE_LOGS,
      appLogs
    )
  }
}
