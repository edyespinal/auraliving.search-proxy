import type { Cached, ClientsConfig } from '@vtex/api'
import { IOClients, LRUCache } from '@vtex/api'

import { GraphQLProxy } from './graphqlProxy'

export class Clients extends IOClients {
  public get graphQLProxy() {
    return this.getOrSet('graphQLProxy', GraphQLProxy)
  }
}

const TIMEOUT_MS = 5000

const memoryCache = new LRUCache<string, Cached>({ max: 5000 })

export const clientsConfig: ClientsConfig<Clients> = {
  implementation: Clients,
  options: {
    default: {
      retries: 2,
      timeout: TIMEOUT_MS,
    },
    graphQLProxy: {
      retries: 2,
      memoryCache,
      timeout: 10000,
    },
  },
}
