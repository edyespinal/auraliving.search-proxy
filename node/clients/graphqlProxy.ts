import type { IOContext, InstanceOptions } from '@vtex/api'
import { JanusClient } from '@vtex/api'

type QueryResponse<T> = {
  data: {
    [queryName: string]: T
  }
}

export class GraphQLProxy extends JanusClient {
  constructor(ctx: IOContext, options?: InstanceOptions) {
    super(ctx, {
      ...options,
      headers: {
        ...(options?.headers ?? {}),
        VtexIdclientAutCookie: ctx.authToken,
        'X-Vtex-Use-Https': 'true',
      },
    })
  }

  public async query<T>(query: string): Promise<QueryResponse<T>> {
    return this.http.post(
      'http://auraliving.myvtex.com/_v/private/graphql/v1',
      {
        query,
      },
      {
        metric: 'graphql-query',
      }
    )
  }
}
