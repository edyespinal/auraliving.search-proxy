import { method } from '@vtex/api'

import { errorHandler } from './errorHandler'
import { query } from './query'
import { getAppLogs } from './getAppLogs'

export const applicationRoutes = {
  query: method({
    POST: [errorHandler, query],
  }),
  appLogs: method({
    GET: [errorHandler, getAppLogs],
  }),
}
