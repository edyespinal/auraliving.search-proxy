import { json } from 'co-body'

import { IOServiceError } from '../../utils/errorHandling/ioServiceError'
import { getQueryInfo } from '../../utils'
import { sortProducts } from '../../utils/sortProducts'
import { filterProductsOutOfPriceRange } from '../../utils/filterProducts'

export async function query(ctx: Context) {
  try {
    const {
      clients: { graphQLProxy },
    } = ctx

    const body = await json(ctx.req, { jsonTypes: ['application/json'] })
    const { queryName, args } = getQueryInfo(body.query)

    const response = await graphQLProxy.query<any>(body.query)
    const data = response.data[queryName]

    if (queryName === 'queryNotSupported' || !data.products) {
      ctx.body = response

      return
    }

    let { products } = data

    products = filterProductsOutOfPriceRange(products, args)

    if (args.orderBy) {
      products = sortProducts(products, args.orderBy)
    }

    ctx.body = {
      data: {
        [queryName]: {
          products,
        },
      },
    }

    return
  } catch (error) {
    throw new IOServiceError({
      message: 'Error completing query',
      status: error.reponse?.status || 400,
      reason: error.message,
      code: 'USER_INPUT_ERROR',
      date: new Date(),
      exception: error,
    })
  }
}
